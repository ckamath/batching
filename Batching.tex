\documentclass[a4paper,10pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Packages
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%BEGIN
\usepackage{amsmath}%Required for \boldsymbol	
\usepackage{hyperref}%Clickable references via \href
%END

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Batching Engine
%Designed based on tex.stackexchange answers 39382, 20099 and 100684
%Note: things are case-senstive
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%BEGIN
\makeatletter
\newcommand\InitiateObject[3]{
%Purpose: Level-1 macro for initiating an object, i.e., it defines the object's standalone (\Declare<ObjectName>) and batch (\Batch<ObjectName>) macros
%Syntax: \InitiateObject{<ObjectName>}{<Prefix>}{<TextType>} 
%E.g.: \InitiateObject{set}{s}{\mathcal}, which allows defining `set' macros of the form \s* (e.g., \sH in the MWE below) using \Declareset command below.
	\expandafter\newcommand\csname Declare#1\endcsname[1]{
	%Purpose: Level-2 macro which defines the standalone command \Declare<ObjectName>
	%Syntax: \Declare<ObjectName>{<Suffix>}
	%E.g.: \Declareset{H}, which defines the macro \sH
	%WARNING: Since \def is used below, an already-existing command will be *overwritten*; for safety, use \newcommand or \providecommand instead.
		\expandafter\def\csname#2##1\endcsname{
		%Purpose: Level-3 macro, that actually defines the instances of an object
		%Syntax: \<Prefix><Suffix>, which is expanded into \TextType{\Suffix}
		%E.g., \sH, which is compiled into \mathcal{H}
			\ensuremath{#3{##1}}
		}
	}
	\expandafter\newcommand\csname Batch#1\endcsname[1]{
	%Purpose: Level-2 macro which defines the batch command \Batch<ObjectName>
	%Syntax: \Batch<ObjectName>{<Suffixes in CSV list>}
	%E.g.: \Batchsets{H,I}, which calls \Declareset{H} and \Declareset{I} which, in turn, defines the macros \sH and \sI.
		\@for\@Suffix:=##1\do{
			\expandafter\expandafter\csname Declare#1\endcsname\expandafter{\@Suffix}
		}
	}
}
\newcommand\InitiateObjects[1]{
	%Purpose: Level-1 macro for batch-initiating objects; works by repeatedly calling \InitiateObject
	%Syntax: \InitiateObjects{<Objects'Description>}, where <Objects'Description> is a CSV list, with each entry in the list being the description of the object as {<ObjectName>}{<Prefix>}{<TextType>}
	%E.g.: \InitiateObjects{{set}{s}{\mathcal},{function}{f}{}} defines a set object and a function object with prefixes s and f respectively.
	\@for\@Object:=#1\do{
		\expandafter\InitiateObject\@Object
	}
}
\makeatother
%%END

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Batching Engine, without comments (for easy copy-pasting)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%BEGIN
%\makeatletter
%\newcommand\InitiateObject[3]{
%	\expandafter\newcommand\csname Declare#1\endcsname[1]{
%		\expandafter\def\csname#2##1\endcsname{
%			\ensuremath{#3{##1}}
%		}
%	}
%	\expandafter\newcommand\csname Batch#1\endcsname[1]{
%		\@for\@Suffix:=##1\do{
%			\expandafter\expandafter\csname Declare#1\endcsname\expandafter{\@Suffix}
%		}
%	}
%}
%\newcommand\InitiateObjects[1]{
%	\@for\@Object:=#1\do{
%		\expandafter\InitiateObject\@Object
%	}
%}
%\makeatother
%END

%%Declare objects required for your paper as a CSV list, with each object of the form {<ObjectName>}{<Prefix>}{<TextType>}.
%It is recommended to use not-too-short prefixes so that existing commands are not overwritten.
\InitiateObjects{
	%Discrete math
	{set}{s}{\mathcal},%Sets in calligraphic font, \s*
	{function}{f}{},%Functions, \f*
	{polynomial}{p}{},%Polynomials, \p*
	{vector}{v}{\boldsymbol},%Vectors, \V*
	%Probability
	{Event}{E}{},%Events, \E*
	{Distribution}{D}{},%\Distributions, \D*
	%TCS
	{Algorithm}{A}{\mathsf},%Algorithms, \A*
	{circuit}{c}{\mathsf},%Circuits, \c*
	{Class}{C}{\mathbf},%Complexity classes, \C*
	{Problem}{P}{\textsc}%Search problems, \P*
}

%Declare instances of objects required for your paper.
%Discrete math
\Batchset{H,I}%Instances of sets: \sH and \sI
\Batchvector{v,m}%Instances of vectors: \vv and \vm
\Batchpolynomial{p}%...\pp
\Batchfunction{trace}%
%Probability objects
\BatchEvent{bad}%\Ebad
\BatchDistribution{X}%\DX
%CS objects
\BatchAlgorithm{A,PP}% \AS and \APP
\Batchcircuit{C}%\cC
\BatchClass{P,NP}%\CP and \CNP
\BatchProblem{Factoring,SVL}%PFactoring and \PSVL

\title{Batching for TCS Papers\footnote{Version 1.1}}
\author{Chethan Kamath}
\date{\today}

\begin{document}
	\maketitle
	
	Often times when writing TCS papers, we need to define multiple instances of the same object, e.g., sets. 
	Instead of defining each instance separately, it is desirable to batch-define them so that there is minimal amount of \LaTeX\ code.
	This note serves a minimal-working example of the \TeX\ code that I ended up converging to, thanks to \cite{TeXSE:egreg11a,TeXSE:egreg11b,TeXSE:egreg13,Manual:Feuersaenger20,Manual:Knuth86}  -- an explanation of the code can be found as comments in the \verb|.tex| file.
	To demonstrate the code, some of the notation from my thesis \cite{Thesis:Kamath20} has been ported to the new macros:
	\begin{itemize}
		\item We use straight font to denote algorithms, circuits and protocols (e.g., $\AA, \cC, \APP$), calligraphic font to denote sets (e.g., $\sI,\sH$), bold face to denote complexity classes (e.g., $\CP, \CNP$) or vectors (e.g., $\vv, \vm$), small caps to denote problems or languages (e.g., $\PFactoring, \PSVL$). Polynomials, func-tions and events are in normal math mode (e.g., $\pp(n), \ftrace, \Ebad$).
	\end{itemize}

	\begin{thebibliography}{1}
		\bibitem{TeXSE:egreg11a}
			{\sc egreg},
			\newblock {\em What exactly do \verb|\csname| and \verb|\endcsname| do?}.
			\newblock \href{https://tex.stackexchange.com/questions/39380/what-exactly-do-csname-and-endcsname-do#39382}{\TeX.Stackexchange answer 39382}, accessed \date{27/12/2023}.
		\bibitem{TeXSE:egreg11b}
			{\sc egreg},
			\newblock {\em Proper way to use \verb|\ensuremath| to define a macro useable in and out of math mode}.
			\newblock \href{https://tex.stackexchange.com/questions/20096/proper-way-to-use-ensuremath-to-define-a-macro-useable-in-and-out-of-math-mode#20099}{\TeX.Stackexchange answer 20099}, accessed \date{27/12/2023}.
		\bibitem{TeXSE:egreg13}
			{\sc egreg},
			\newblock {\em \LaTeX\ for loop \verb|\@for|}.
			\newblock \href{https://tex.stackexchange.com/questions/100633/latex-for-loop-for#100684}{\TeX.Stackexchange answer 100684}, accessed \date{27/12/2023}.
		\bibitem{Manual:Feuersaenger20}
			{\sc Feuersänger, C.},
			\newblock {\em Notes On Programming in \TeX}.
			\newblock Revision 1.18.1, 2021.
		\bibitem{Thesis:Kamath20}
			{\sc Kamath, C.},
			\newblock {\em On the Average-Case Hardness of Total Search Problems}.
			\newblock PhD thesis, IST Austria, 2020.
		\bibitem{Manual:Knuth86}
			{\sc Knuth, D.},
			\newblock {\em The \TeX book}.
			\newblock 1986.
	\end{thebibliography}

	\appendix
	
	\section{Versions}
	\begin{enumerate}
		\item \emph{Version 1.1}: Implemented loops using native \TeX\ command \verb|\@for| (which slightly changes the syntax of \verb|\InitiateObjects| macro).
		\item \emph{Version 1.0}: Supports basic batching of objects, with loops implemented using \verb|\forcsvlist| from \texttt{etoolbox} package.
	\end{enumerate}
\end{document}